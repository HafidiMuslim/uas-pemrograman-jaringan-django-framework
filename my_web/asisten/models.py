from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone

# Create your models here.

class Asisten(models.Model):
	KATEGORI_CHOICES = (
		('sp','Sopir'),
		('ss','Suster'),
		('as','Asisten'),
	)
	nama_pekerja = models.CharField('Nama Pekerja',max_length=50,null=False)
	kategori = models.CharField(max_length=2,choices=KATEGORI_CHOICES)
	alamat_rumah = models.TextField()
	usia = models.FloatField('Usia (th)')
	pengalaman = models.FloatField('Pengalaman (th)')
	gaji = models.IntegerField()
	tgl_input = models.DateTimeField('Tgl. Edit',default=timezone.now)
	user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

	class Meta:
		ordering = ['-tgl_input']

	def __str__(self):
		return self.nama_pekerja

	def get_absolute_url(self):
		return reverse('home_page')